from __future__ import division

import netCDF4
import pylab as pl
import numpy as np
import pandas as pd
import warnings
import pycwt as wavelet
from pycwt.helpers import find
from functions import anglemean

warnings.filterwarnings("ignore",category = RuntimeWarning) 

shifts    = pd.read_pickle("data_plot_1.pickle") 
amo       = pd.read_pickle("data_plot_1_amo.pickle").values.squeeze()
nao       = shifts["NAO (EOF 1)"].values.squeeze()*-1

mother    = "morlet"
dt        = 1
t0        = 950
s0        = 2 * dt
dj        = 1 / 12
J         = int(7 / dj)
n         = len(amo)
t         = np.arange(n) * dt + t0

#
# CWT
#
std = amo.std()  # Standard deviation
variance = std ** 2  # Variance
var_norm = amo / std  # Normalized dataset

alpha, _ , _ , = wavelet.ar1(var_norm) 

wave, scales, freqs, coi, fft, fftfreqs = wavelet.cwt(var_norm, dt, 
                                                 dj, s0, J, wavelet=mother)
iwave = wavelet.icwt(wave, scales, dt, dj, mother) * std   

power_spectra = (np.abs(wave)) ** 2
fft_power = np.abs(fft) ** 2
period = 1 / freqs
signif, fft_theor = wavelet.significance(1.0, dt, scales, 0, alpha,
                                        significance_level=0.95,
                                        wavelet=mother)
sig95 = np.ones([1, n]) * signif[:, None]
sig95 = power_spectra / sig95

WCT, aWCT, corr_coi, freq, sig = wavelet.wct(amo, nao, dt,
                                            dj=dj, s0=s0, J=J,
                                            significance_level=0.8646,
                                            wavelet=mother, normalize=True,
                                            cache=False, mc_count = 500)


angle = aWCT.copy()
angle_orig = np.ma.asarray(aWCT.copy())

cor_sig = np.ones([1, n]) * sig[:, None]
cor_sig = np.abs(WCT) / cor_sig     # Power is significant where ratio > 1
cor_period = 1 / freq

# Calculates the phase between both time series. The phase arrows in the
# cross wavelet power spectrum rotate clockwise with 'north' origin.
# The relative phase relationship convention is the same as adopted
# by Torrence and Webster (1999), where in phase signals point
# upwards (N), anti-phase signals point downwards (S). If X leads Y,
# arrows point to the right (E) and if X lags Y, arrow points to the
# left (W).
# Phase relationship

u, v = np.cos(angle), np.sin(angle)  

u_sig = u.copy()                    # Mask everything that's not significant
v_sig = v.copy()

u_sig[cor_sig < 1] = np.ma.masked
v_sig[cor_sig < 1] = np.ma.masked

[t1_sig ,cor_period_sig]    = np.meshgrid(t,cor_period)
t1_sig[cor_sig < 1]         = np.ma.masked
cor_period_sig[cor_sig < 1] = np.ma.masked
angle_orig[cor_sig<1]       = np.ma.masked
angle_orig[sig95<1]         = np.ma.masked

sel = find((period >= 90) & (period < 180))
Cdelta    = wavelet.Morlet(6).cdelta
scale_avg = (scales * np.ones((n, 1))).transpose()
scale_avg = power_spectra / scale_avg  
scale_avg_mca = variance * dj * dt / Cdelta * scale_avg[sel, :].sum(axis=0)
scale_avg_signif_mca, tmp = wavelet.significance(amo, dt, scales, 2, alpha,
                                             significance_level=0.95,
                                             dof=[scales[sel[0]],
                                                  scales[sel[-1]]],
                                             wavelet=mother)

sel = find((period >= 60) & (period < 90))
Cdelta    = wavelet.Morlet(6).cdelta
scale_avg = (scales * np.ones((n, 1))).transpose()
scale_avg = power_spectra / scale_avg  
scale_avg_lia = variance * dj * dt / Cdelta * scale_avg[sel, :].sum(axis=0)
scale_avg_signif_lia, tmp = wavelet.significance(amo, dt, scales, 2, alpha,
                                             significance_level=0.95,
                                             dof=[scales[sel[0]],
                                                  scales[sel[-1]]],
                                             wavelet=mother)

sel = find((period >= 90) & (period < 180))
Cdelta    = wavelet.Morlet(6).cdelta
scale_avg = angle_orig[sel,:] 
scale_avg_mca_ang, _, _, _, _ = anglemean(scale_avg)


sel = find((period >= 60) & (period < 90))
Cdelta    = wavelet.Morlet(6).cdelta
scale_avg = angle_orig[sel,:]
scale_avg_lia_ang, _, _, _, _ = anglemean(scale_avg)


##
# Rescale to years
#

scale_avg_lia_ang[scale_avg_lia_ang>0] = scale_avg_lia_ang[scale_avg_lia_ang>0] 
scale_avg_mca_ang[scale_avg_mca_ang>0] = scale_avg_mca_ang[scale_avg_mca_ang>0] 

scale_avg_mca_ang = (scale_avg_mca_ang) / np.pi / 2 * ((90+180)/2)
scale_avg_lia_ang = (scale_avg_lia_ang) / np.pi / 2 * ((90+60)/2)

scale_avg_signif_mca_ang = scale_avg_mca_ang[100:450].mean()
scale_avg_signif_lia_ang = scale_avg_lia_ang[500:-100].mean()
#
# WTC
#


f = pl.figure(figsize=(11,5))

bx = pl.axes([0.1, 0.5, 0.35, 0.45])
bx.set_title('(a) AMO Wavelet Power Spectrum (Morlet)')

levels = [0.0625, 0.125, 0.25, 0.5, 1, 2, 4, 8, 16]
im = bx.contourf(t, np.log2(period), np.log2(power_spectra), np.log2(levels),
            alpha=0.8, extend='both', cmap=pl.cm.Blues)
extent = [t.min(), t.max(), 0, max(period)]
bx.contour(t, np.log2(period), sig95, [-99, 1], colors='k', linewidths=2,
        extent=extent)
bx.fill(np.concatenate([t, t[-1:] + dt, t[-1:] + dt,
                        t[:1] - dt, t[:1] - dt]),
        np.concatenate([np.log2(coi), [1e-9], np.log2(period[-1:]),
                        np.log2(period[-1:]), [1e-9]]),
        'k', alpha=0.3, hatch='x')

bx.set_ylabel('Period (years)')
bx.set_aspect('auto')
bx.set_xlim([950, 1800])
Yticks = 2 ** np.arange(np.ceil(np.log2(period.min())),
                        np.ceil(np.log2(period.max())),0.5)
bx.set_yticks(np.log2(Yticks))
bx.set_yticklabels(Yticks.astype(int))
bx.set_ylim([np.log2(period).min(), np.log2(period).max()])
bx.invert_yaxis()

#
# Subplot 1
#

cx = pl.axes([0.5, 0.5, 0.35, 0.45], sharey=bx)

cor_sig = np.ones([1, n]) * sig[:, None]
cor_sig = np.abs(WCT) / cor_sig     # Power is significant where ratio > 1
cor_period = 1 / freq

#
# Plotting
#
uv = [u , v]
uv_sig = [u_sig, v_sig]

levels = np.linspace(0,1,11)
cx.set_title('(c) Wavelet coherence AMO and {}'.format("NAO EOF(1)"))
im2 = cx.contourf(t, np.log2(cor_period)[1:90], WCT[1:90], cmap=pl.cm.Blues, levels=levels, antialiased=True, alpha=0.8)
cx.contour(t, np.log2(cor_period[1:90]), cor_period_sig[1:90], [-99, 1], colors='k', linewidths=2)
cx.quiver(t1_sig[35:90][::3,::30], np.log2(cor_period_sig[35:90][::3,::30]), uv_sig[0][35:90][::3,::30],uv_sig[1][35:90][::3,::30], units='height',\
angles='uv', pivot='mid', linewidth=1, edgecolor='k',scale=35,scale_units="height",
headwidth=5, headlength=5, headaxislength=2, minshaft=2,
minlength=2)
cx.fill(np.concatenate([t, t[-1:] + dt,\
        t[-1:] + dt,
        t[:1] - dt, t[:1] - dt]),
        np.concatenate([np.log2(corr_coi), [1e-9],\
        np.log2(cor_period[-1:]),
        np.log2(cor_period[-1:]), [1e-9]]),
        'k', alpha=0.7, hatch='x')

Yticks = 2 ** np.arange(np.ceil(np.log2(period.min())),
                        np.ceil(np.log2(period.max())),0.5)
cx.set_yticks(np.log2(Yticks))
cx.set_yticklabels(Yticks.astype(int))
cx.set_ylim([np.log2(period).min(), np.log2(period).max()])
cx.invert_yaxis()
#cx.set_ylabel('Period (years)')

cx.set_xlim([950, 1800])

dx = pl.axes([0.1, 0.1, 0.35, 0.3])


#dx.axhline(scale_avg_signif_mca, color='r', linestyle='--', linewidth=1.)
dx.plot(t, scale_avg_mca, 'r-', linewidth=1.5, alpha=0.5, label="90-180 yr")
#dx.axhline(scale_avg_signif_lia, color='b', linestyle='--', linewidth=1.)
dx.plot(t, scale_avg_lia, 'b-', linewidth=1.5, alpha = 0.5, label="60-90 yr")
dx.legend(loc="upper left", frameon=False)
dx.set_title('(b) scale-averaged power during MCA and LIA')
dx.set_xlabel('Time (year)')
dx.set_ylabel(r'Average variance [{}]'.format('degC'))
dx.set_xlim([t.min(), t.max()])
dx.axvline(x=1150 ,color='black', linestyle='--')
dx.axvline(x=1400, color='black', linestyle='--')
#dx.text(1200, -0.05, "MCA")
dx.axvline(x=1450 ,color='black', linestyle='--')
dx.axvline(x=1700, color='black', linestyle='--')
#dx.text(1550, -0.05, "LIA")


ex_ = pl.axes([0.5, 0.1, 0.35, 0.3])
ex = ex_.twinx()
ex.axhline(0, color='k', linestyle='--', linewidth=1.)
ex.plot(t[500:750], scale_avg_lia_ang[500:750], 'b-', linewidth=1.5, alpha = 0.5, label="60-90yr")
ex.plot(t[200:450], scale_avg_mca_ang[200:450], 'r-', linewidth=1.5, alpha = 0.5, label="90-180yr")
#ex.axhline(scale_avg_signif_mca_ang, color='b', linestyle='--', linewidth=1.)

ex.axvline(x=1150 ,color='black', linestyle='--')
ex.axvline(x=1400, color='black', linestyle='--')
#ex.text(1200, -40, "MCA")
ex.axvline(x=1450 ,color='black', linestyle='--')
ex.axvline(x=1700, color='black', linestyle='--')
#ex.text(1550, -40, "LIA")
ex.legend(loc="upper left", frameon=False)
ex.set_title('(d) scale weighted phase relationship during MCA and LIA')
ex.set_xlabel('Time (year)')
#ex.set_yticks([-3.14, -3.14*0.5, 0, 3.14*0.5, 3.14])
#ex.set_yticklabels(["-$\pi$", "-$\pi$/2", 0, "$\pi$/2", "$\pi$"])
ex.set_ylabel(r'Phaselag in years')
ex.set_xlim([t.min(), t.max()])
ex_.set_yticks([])
#ex.set_ylim([-10, 30])
ex.set_yticks([-20,-15,-10,-5,0,5,10,15,20])

cbar_ax = f.add_axes([0.875, 0.6, 0.025, 0.3])
cbar = f.colorbar(im2, cax=cbar_ax)
cbar.set_ticks([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
cbar.ax.set_ylabel('Coherence')

pl.savefig('figure_1_paper_wavelet.png', dpi = 300)
pl.show()
pl.close()
