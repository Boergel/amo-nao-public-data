import xarray as xr
import numpy as np
import cftime

from eofs.standard import Eof
import numpy as np   
import pandas as pd
from sklearn.cluster import KMeans
import math
import scipy


class eof_init():
        
    def __init__(self, ds):
        """Loads initial parameters of xarray dataset. 
        And stores them in object.

        Args:
            ds (xarray dataset): Dataset containing
            sea level pressure, longitude, latitude,
            time.
        """
        
        
        self.ds = ds
        self.slp = ds.sea_level_pressure
        self.lon = ds.lon
        self.lat = ds.lat
        
    def mean2(self, x):
        """Used for the corr2 function, according to Matlab.
        
        Args:
            x (float): 
            
        Returns:
            y (float): mean of x
        """
        y = np.sum(x) / np.size(x);
        return y

    def corr2(self,a,b):
        """
        According to Matlab corr2-function.
        """
        a = a - self.mean2(a)
        b = b - self.mean2(b)

        r = (a*b).sum() / math.sqrt((a*a).sum() * (b*b).sum());
        return r
        
    def interpolate_data(self, interpolation_method = 1):
        """Interpolate missing values in time. Multidimensional arrays are
        reshaped into 1d arrays, so that the fit in a pandas dataFrame.

        Arguments:
            var {float} -- Multidimensional array - first dimension must be time

        Keyword Arguments:
            interpolation_method {int} -- 1: running mean, 2: linear, 3: quadratic, 4: nearest (default: {1})
            verbose {bool} -- Prints example of interpolated values, currently only for oetzi run defined (default: {False})

        Returns:
            float -- NaN corrected multi dimensional array
        """
    
        var_corrected = self.ds.sea_level_pressure.values.copy()

        if interpolation_method == 1:
            masked_values = np.argwhere(np.isnan(var_corrected.reshape(len(var_corrected), -1)[:,0]))       # Identify missing values
            if masked_values.any() is not None:
                for interp in masked_values.squeeze():
                    if interp < 50:
                        var_corrected[interp] = np.nanmean(self.slp.values[0:50+interp], axis = 0)
                    else:
                        var_corrected[interp] = np.nanmean(self.slp.values[interp-50:interp+50], axis = 0)

            self.ds['sea_level_pressure'].values = var_corrected.copy()

        if interpolation_method == 2:
            var_corrected = var_corrected.reshape(len(var_corrected), -1)

            for i in range(len(var_corrected[0])):
                tmp = var_corrected[:, i]
                dataFrame_var = pd.DataFrame(tmp,columns=['Mean SST Atlantic'])
                dataFrame_var = dataFrame_var.interpolate(method="linear")
                var_corrected[:, i] = dataFrame_var.values.squeeze()

            self.ds['sea_level_pressure'].values = var_corrected.reshape(var.shape)

        if interpolation_method == 3:
            var_corrected = var_corrected.reshape(len(var_corrected), -1)

            for i in range(len(var_corrected[0])):
                tmp = var_corrected[:, i]
                dataFrame_var = pd.DataFrame(tmp,columns=['Mean SST Atlantic'])
                dataFrame_var = dataFrame_var.interpolate(method="quadratic")
                var_corrected[:, i] = dataFrame_var.values.squeeze()

            self.ds['sea_level_pressure'].values = var_corrected.reshape(var.shape)

        if interpolation_method == 4:
            var_corrected = var_corrected.reshape(len(var_corrected), -1)

            for i in range(len(var_corrected[0])):
                tmp = var_corrected[:, i]
                dataFrame_var = pd.DataFrame(tmp,columns=['Mean SST Atlantic'])
                dataFrame_var = dataFrame_var.interpolate(method="nearest")
                var_corrected[:, i] = dataFrame_var.values.squeeze()

            self.ds['sea_level_pressure'].values = var_corrected.reshape(var.shape)
            
        print("ds['sea_level_pressure'] updated")


    def calc_eof(self, eof_number):
        self.ds['slp_anom'] = self.ds['sea_level_pressure'] - self.ds['sea_level_pressure'].mean()
        coslat = np.cos(np.deg2rad(self.ds.lat.values)).clip(0., 1.)
        wgts = np.sqrt(coslat)[..., np.newaxis]
        solver = Eof(self.ds.slp_anom.values, weights=wgts)     
        eof1 = solver.eofsAsCovariance(pcscaling=0, neofs=eof_number).squeeze()
        
        for number, eof in enumerate(eof1): 
            foo = xr.DataArray(eof, coords=[self.ds.lat, self.ds.lon])
            self.ds["EOF{}".format(number)] = foo
        
        pc1 = solver.pcs(npcs=eof_number, pcscaling=1).squeeze() # Unit variance

        for number in range(0,len(pc1[0])):
            foo = xr.DataArray(pc1[:,number], coords=[self.ds.time])
            self.ds["PC{}".format(number)] = foo
        
        self.variance_fractions = solver.varianceFraction()
        
        
    def calc_center_kMeans(self, reference_eof, eof_number = 4, n = 15):
        """
        update for number of eofs = 1
        """
        
        for count in range(0, eof_number):
            
            tmp_clust = self.ds["EOF{}".format(count)].values.reshape(-1,1)
            k_means = KMeans(n_clusters=n, random_state= 0)
            _ = k_means.fit(tmp_clust)
            X_clustered = k_means.labels_
            X_clustered = X_clustered.reshape(self.ds["EOF{}".format(count)].values.shape)


            label_list = [self.ds["EOF{}".format(count)].values[X_clustered == i].mean() for i in np.arange(0,n)]
            mean_sort = np.argsort(np.asarray(label_list))

            new = X_clustered.copy().astype(float)

            for step, i in enumerate(mean_sort):
                fac = step * 0.1
                new[X_clustered == i] = fac
            new = (new * 10).astype(int)
            
            foo = xr.DataArray(new, coords=[self.ds.lat, self.ds.lon])

            corr = self.corr2(self.ds["EOF{}".format(count)].values, reference_eof)
            if corr >= 0.75:
                foo_tmp = foo.where(foo == 0, drop = True)
                self.minimum = foo_tmp.lat.mean().values, foo_tmp.lon.mean().values
                foo_tmp = foo.where(foo == n-1, drop = True)
                self.maximum = foo_tmp.lat.mean().values, foo_tmp.lon.mean().values
                break
            elif corr <= -0.75:
                foo_tmp = foo.where(foo == n-1, drop = True)
                self.minimum = foo_tmp.lat.mean().values, foo_tmp.lon.mean().values
                foo_tmp = foo.where(foo == 0, drop = True)
                self.maximum = foo_tmp.lat.mean().values, foo_tmp.lon.mean().values
                break
            else:
                self.minimum = np.nan, np.nan
                self.maximum = np.nan, np.nan

class amo():
    
    def __init__(self,begin,end,contains_nan = False, do_roll = True,
                 amo_global = True):

        
        sst_global = xr.open_dataset("../data/oetzi2_sst_yearmean.nc")
        amo        = xr.open_dataset("../data/oetzi2_sst_-80_0_0_60.nc")
    
        if amo_global == True:
            amo_detrended = amo.var169 - sst_global.var169
            amo_detrended.name = "amo"
        else:
            amo_detrended = amo.var169
            amo_detrended.name = "amo"
        
        
        self.ds = amo_detrended.copy()
        self.correct_time()
        self.ds = self.ds.sel(time=slice(str(begin), str(end)))
        
        if contains_nan == True:
            self.interpolate_data()
        
        if do_roll == True:
            self.ds = self.ds.rolling(time = 10, center = False, min_periods = 1).mean()
            
        self.ds = self.ds - self.ds.mean()
        self.ds = self.ds.mean(["lon", "lat"])
        
        
        self.ds = xr.Dataset({"amo": (["time"], self.ds.values)},
                               coords={"time": (["time"], self.ds.time)}).groupby("time.year").mean()
    
        
    def interpolate_data(self):
        var_corrected = self.ds.values.copy()
        masked_values = np.argwhere(np.isnan(self.ds.values.reshape(len(self.ds.values), -1)[:,0]))       # Identify missing values
        if masked_values.any() is not None:
            if masked_values.squeeze().size == 1:
                if int(masked_values.squeeze()) < 50:
                    var_corrected[int(masked_values.squeeze())] = np.nanmean(self.ds.values[0:50+int(masked_values.squeeze())], axis = 0)
                else:
                    var_corrected[int(masked_values.squeeze())] = np.nanmean(self.ds.values[int(masked_values.squeeze())-50:int(masked_values.squeeze())+50], axis = 0)
            else:
                for interp in masked_values.squeeze():
                    if interp < 50:
                        var_corrected[interp] = np.nanmean(self.ds.values[0:50+interp], axis = 0)
                    else:
                        var_corrected[interp] = np.nanmean(self.ds.values[interp-50:interp+50], axis = 0)

        self.ds.values = var_corrected
    
    def correct_time(self):
        tmp = []
        for count, step in enumerate(self.ds.time.values):
            tmp.append(cftime.DatetimeProlepticGregorian(int(str(step)[0:4]),
                                                         int(str(step)[4:6]),
                                                         int(str(step)[6:8])))
        self.ds.time.values = tmp
            