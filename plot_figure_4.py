import numpy as np
import pandas as pd
import xarray as xr
import cftime
import pylab as plt

import cartopy.crs as ccrs

import matplotlib

storm_tracks = xr.open_dataset("storm_tracks.nc")

offset = 1500 # simulation starts at 1500 ...

ds_mca = storm_tracks.sel(year=slice(5950+offset,6250+offset))
ds_lia = storm_tracks.sel(year=slice(6300+offset,6799+offset))


amo_full = xr.open_dataset("amo_full.nc")

amo_time_mca = amo_full.amo.sel(year=slice(5950+offset, 6250+offset))
amo_time_lia = amo_full.amo.sel(year=slice(6300+offset, 6799+offset))

std_mca = amo_full.amo.std()
threshold = 0.25 * std_mca 

std_lia = amo_full.amo.std()
threshold_lia = 0.25 * std_lia 

           
amo_plus_mca  = amo_time_mca[amo_time_mca > threshold]
amo_minus_mca = amo_time_mca[amo_time_mca < -threshold]

amo_plus_lia  = amo_time_lia[amo_time_lia > threshold_lia]
amo_minus_lia = amo_time_lia[amo_time_lia < -threshold_lia]

dset_rca3 = xr.open_dataset("dset_rca3.nc")
lon_rca3, lat_rca3 = dset_rca3['lon'][:], dset_rca3['lat'][:]

dset_rca3_mca = dset_rca3.sel(year=slice("0950","1250"))
dset_rca3_lia = dset_rca3.sel(year=slice("1300","1800"))

dset_rca3_mca.year.values = amo_time_mca.year.values
dset_rca3_lia.year.values = amo_time_lia.year.values

u_amo_plus_mca = dset_rca3_mca.u[amo_time_mca > threshold]
u_amo_minus_mca = dset_rca3_mca.u[amo_time_mca < -threshold]

v_amo_plus_mca = dset_rca3_mca.v[amo_time_mca > threshold]
v_amo_minus_mca = dset_rca3_mca.v[amo_time_mca < -threshold]

u_amo_plus_lia = dset_rca3_lia.u[amo_time_lia > threshold_lia]
u_amo_minus_lia = dset_rca3_lia.u[amo_time_lia < -threshold_lia]

v_amo_plus_lia = dset_rca3_lia.v[amo_time_lia > threshold_lia]
v_amo_minus_lia = dset_rca3_lia.v[amo_time_lia < -threshold_lia]



u_diff_mca = u_amo_plus_mca.mean("year") - u_amo_minus_mca.mean("year")
v_diff_mca = v_amo_plus_mca.mean("year") - v_amo_minus_mca.mean("year")

u_diff_lia = u_amo_plus_lia.mean("year") - u_amo_minus_lia.mean("year")
v_diff_lia = v_amo_plus_lia.mean("year") - v_amo_minus_lia.mean("year")

u_mean_mca = dset_rca3_mca.u.mean("year")
v_mean_mca = dset_rca3_mca.v.mean("year")

u_mean_lia = dset_rca3_lia.u.mean("year")
v_mean_lia = dset_rca3_lia.v.mean("year")

mag_uv_mca = np.sqrt(u_mean_mca**2 + v_mean_mca**2)
mag_uv_diff_mca = np.sqrt(u_diff_mca**2 + v_diff_mca**2)

mag_uv_lia = np.sqrt(u_mean_lia**2 + v_mean_lia**2)
mag_uv_diff_lia = np.sqrt(u_diff_lia**2 + v_diff_lia**2)


LONS, LATS = np.meshgrid(lon_rca3, lat_rca3)

u_mean_mca.plot()


from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.ticker as mticker
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42


grid = plt.GridSpec(2, 2, wspace=0.3, hspace=0.4)

fig = plt.figure(figsize=(10, 15))
grid = plt.GridSpec(3, 1, hspace=0.2, wspace=0.1)
ax1 = fig.add_subplot(grid[0], projection= ccrs.PlateCarree())
ax2 = fig.add_subplot(grid[1], projection= ccrs.PlateCarree())
ax3 = fig.add_subplot(grid[2], projection= ccrs.PlateCarree())


storm_tracks_m = storm_tracks.mean("year")
normed_stormtracks = (storm_tracks_m.stormtrack_density - storm_tracks_m.stormtrack_density.min()) /  (storm_tracks_m.stormtrack_density.max() - storm_tracks_m.stormtrack_density.min())
ds_mca_yr = ds_mca.mean("year")
ds_lia_yr = ds_lia.mean("year")

ds_mca_yr.stormtrack_density.values = (-ds_mca_yr.stormtrack_density.min() + ds_mca_yr.stormtrack_density)/(ds_mca_yr.stormtrack_density.max()-ds_mca_yr.stormtrack_density.min())
ds_lia_yr.stormtrack_density.values = (-ds_lia_yr.stormtrack_density.min() + ds_lia_yr.stormtrack_density)/(ds_lia_yr.stormtrack_density.max()-ds_lia_yr.stormtrack_density.min())

diff_mca_lia = ds_mca_yr - ds_lia_yr


amo_plus_centers_mca = ds_mca.stormtrack_density.loc[dict(year=amo_plus_mca.year)].mean("year")
amo_minus_centers_mca = ds_mca.stormtrack_density.loc[dict(year=amo_minus_mca.year)].mean("year")
amo_plus_centers_lia = ds_lia.stormtrack_density.loc[dict(year=amo_plus_lia.year)].mean("year")
amo_minus_centers_lia = ds_lia.stormtrack_density.loc[dict(year=amo_minus_lia.year)].mean("year")


amo_plus_centers_mca = (-amo_plus_centers_mca.min() + amo_plus_centers_mca) / (amo_plus_centers_mca.max()-amo_plus_centers_mca.min())
amo_plus_centers_lia = (-amo_plus_centers_lia.min() + amo_plus_centers_lia) / (amo_plus_centers_lia.max()-amo_plus_centers_lia.min())
amo_minus_centers_mca = (-amo_minus_centers_mca.min() + amo_minus_centers_mca) / (amo_minus_centers_mca.max()-amo_minus_centers_mca.min())
amo_minus_centers_lia = (-amo_minus_centers_lia.min() + amo_minus_centers_lia) / (amo_minus_centers_lia.max()-amo_plus_centers_lia.min())

central_lon, central_lat = -10, 45

diff_mca = (amo_plus_centers_mca - amo_minus_centers_mca)*100
diff_lia = (amo_plus_centers_lia - amo_minus_centers_lia)*100


im = ax1.contourf(storm_tracks.lon, storm_tracks.lat,normed_stormtracks *100, 
               transform=ccrs.PlateCarree(),levels = np.linspace(0,(normed_stormtracks*100).max(),20),cmap=plt.cm.viridis)

diff_lia.plot.contourf(ax = ax3,levels=20,add_colorbar=False)
im2 = diff_mca.plot.contourf(ax = ax2,levels=20, add_colorbar=False)


ax1.quiver(LONS[::3,::6], LATS[::3,::6], u_mean_mca[::3,::6], v_mean_mca[::3,::6])
ax2.quiver(LONS[::3,::6], LATS[::3,::6], u_diff_mca[::3,::6], u_diff_mca[::3,::6])
ax3.quiver(LONS[::3,::6], LATS[::3,::6], u_diff_lia[::3,::6], u_diff_lia[::3,::6])

for count, axis in enumerate([ax1,ax2,ax3]):
    axis.set_aspect("auto")
    axis.coastlines(resolution='50m')

    gl = axis.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                  linewidth=2, color='gray', alpha=0.7, linestyle='--')
    
    if count == 0:
        gl.xlabels_bottom = False
        gl.ylabels_right = True
        
    if count == 1:
        gl.xlabels_bottom = False
        gl.ylables_right = False
    if count == 2:
        gl.ylabels_right = True
        
    axis.set_extent([-30,45,40,65])
    
    gl.xlabels_top = False
    gl.ylabels_left = False
    gl.xlines = True
    gl.xlocator
    gl.xlocator = mticker.FixedLocator([-30, -15, 0,15,30, 45,])
    gl.ylocator = mticker.FixedLocator([40,45,50,55,60,65,70])

    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size':15, 'color': 'gray', "weight": "bold"}
    gl.xlabel_style = {'size':15, 'color': 'gray', 'weight': 'bold'}

fig.subplots_adjust(right=0.8)
cbar_ax1    = fig.add_axes([0.85, 0.70, 0.025, 0.15])
cbar_ax2    = fig.add_axes([0.85, 0.2, 0.025, 0.35])
cbar1       = fig.colorbar(im,ticks=[0,10,20,30,40,50,60,70,80,90,100], cax=cbar_ax1, orientation="vertical")
cbar2       = fig.colorbar(im2, cax=cbar_ax2, orientation="vertical")
cbar1.ax.set_title(r'$\%$')
cbar2.ax.set_title(r'$\%$')

plt.savefig("storm_tracks.png", dpi = 300)

