def interpolate_data(var, interpolation_method = 1, verbose=False):
    """
    Interpolate missing values in time. Multidimensional arrays are
    reshaped into 1d arrays, so that the fit in a pandas dataFrame.

    Arguments:
        var {float} -- Multidimensional array - first dimension must be time
    
    Keyword Arguments:
        interpolation_method {int} -- 1: running mean, 2: linear, 3: quadratic, 4: nearest (default: {1})
        verbose {bool} -- Prints example of interpolated values, currently only for oetzi run defined (default: {False})
    
    Returns:
        float -- NaN corrected multi dimensional array
    """

    import numpy as np
    import pandas as pd
    
    var_corrected = var.copy()

    if interpolation_method == 1:
        masked_values = np.where(var_corrected.reshape(len(var_corrected), -1)[:,0].mask)       # Identify missing values
        for interp in masked_values[0]:
            if interp < 40:
                var_corrected[interp] = np.ma.mean(var[0:40+interp], axis = 0)
            else:
                var_corrected[interp] = np.ma.mean(var[interp-40:interp+40], axis = 0)

    if interpolation_method == 2:
        var_corrected = var_corrected.reshape(len(var_corrected), -1)
        
        for i in range(len(var_corrected[0])):
            tmp = var_corrected[:, i]
            dataFrame_var = pd.DataFrame(tmp,columns=['Mean SST Atlantic'])
            dataFrame_var = dataFrame_var.interpolate(method="linear")
            var_corrected[:, i] = dataFrame_var.values.squeeze()

        var_corrected = var_corrected.reshape(var.shape)

    if interpolation_method == 3:
        var_corrected = var_corrected.reshape(len(var_corrected), -1)
        
        for i in range(len(var_corrected[0])):
            tmp = var_corrected[:, i]
            dataFrame_var = pd.DataFrame(tmp,columns=['Mean SST Atlantic'])
            dataFrame_var = dataFrame_var.interpolate(method="quadratic")
            var_corrected[:, i] = dataFrame_var.values.squeeze()

        var_corrected = var_corrected.reshape(var.shape)

    if interpolation_method == 4:
        var_corrected = var_corrected.reshape(len(var_corrected), -1)
        
        for i in range(len(var_corrected[0])):
            tmp = var_corrected[:, i]
            dataFrame_var = pd.DataFrame(tmp,columns=['Mean SST Atlantic'])
            dataFrame_var = dataFrame_var.interpolate(method="nearest")
            var_corrected[:, i] = dataFrame_var.values.squeeze()

        var_corrected = var_corrected.reshape(var.shape)

    if verbose == True:
        import pylab as pl
        f, ax = pl.subplots(4,5)
        ax = ax.ravel()
        for i, index in enumerate(range(200,220)):
            ax[i].contourf(var_corrected[index])
        pl.show()

    return var_corrected

def make_basemap(dset, lon = "xt_ocean", lat = "yt_ocean", reverse = False, option = "cyl", lon0 = 0., lat0 = 67.0):
    """
    Creates basemap object with boundaries that correspond to lon, lat coordinates in netCDF4
    file.
    
    Arguments:
        dset {netCDF4 dataset object} -- netCDF4 file
    
    Keyword Arguments:
        lon {str} -- Name of longitude component in netCDF4 file (default: {"xt_ocean"})
        lat {str} -- Name of latitude component in netCDF4 file (default: {"yt_ocean"})
        reverse {bool} -- Set to true if reverse latitude component in netCDF4 file (default: {False})
        option {str} -- Projection of basemap object (default: {"cyl"})
        lon0 {[type]} -- if option=ortho defines longitude center of map (default: {0.})
        lat0 {float} -- if option=ortho defines latitude center of map (default: {67.0})
    
    Returns:
        basemap object -- basemap object
    """

    from mpl_toolkits.basemap import Basemap
    import numpy as np

    lon = dset.variables[lon][:]
    if reverse == True:
        lat = dset.variables[lat][:]
        lat = lat[::-1]
    else:
        lat = dset.variables[lat][:]   

    LON, LAT = np.meshgrid(lon, lat)
    
    if option == 'cyl':

        BA_LON = [lon.min(),lon.max()]
        BA_LAT = [lat.min(),lat.max()]
        cp = (.710,.75,.75)

        ba = Basemap(projection='cyl',    
                resolution= 'h',         
                    area_thresh=100.,     
                    lat_0=35.00,           
                    lon_0=20.0,       
                    lat_1=37.75,            
                    llcrnrlon=BA_LON[0],          
                    llcrnrlat=BA_LAT[0],           
                    urcrnrlon=BA_LON[1],                
                    urcrnrlat=BA_LAT[1])
    if option == 'ortho':

        from mpl_toolkits.basemap import Basemap
        import matplotlib.pyplot as plt

        fig = plt.figure(figsize = (5,5))

        ba = Basemap(projection = 'ortho', lon_0 = lon0, lat_0 = lat0)

        BA_LON = [lon.min()-5,lon.max()+5]
        BA_LAT = [lat.min()-5,lat.max()+5]

        tmp1 = ba(BA_LON[0], BA_LON[1])
        tmp2 = ba(BA_LAT[0], BA_LAT[1])
        
        x_range = [tmp1[0], tmp1[1]]
        y_range = [tmp2[0], tmp2[1]]

        return x_range, y_range, ba
       
    return LON, LAT, ba

def do_eof(dset, slp,  
            do_anomaly = True, 
            eof_number = 4,
            long_name = 'lon', 
            lat_name = 'lat'):
    """
    Calculates n eofs of a given variable. Currently defined for reversed latitude.
    Latitude weights are applied.
    
    Arguments:
        dset {netCDF4 file} -- netCDF4 object
        slp {float} -- input variable (dimension: time, lat, lon)
    
    Keyword Arguments:
        do_anomaly {bool} -- If true, anomaly is calculated (default: {True})
        eof_number {int} -- Number of eofs that are returned (default: {4})
        long_name {str} -- Name of longitude component in netCDF4 file (default: {'lon'})
        lat_name {str} -- Name of latitude component in netCDF4 file (default: {'lat'})
    
    Returns:
        eof, variance_fractions, pc1 -- EOFS, explained variance, and principal component
    """


    import netCDF4
    import numpy as np
    from eofs.standard import Eof

    lon = dset.variables[long_name][:]
    lat = dset.variables[lat_name][:]
    lat = lat[::-1]

    if do_anomaly == True:
        slp = slp - np.nanmean(slp, axis=0)

    # Create an EOF solver to do the EOF analysis. Square-root of cosine of
    # latitude weights are applied before the computation of EOFs.

    coslat = np.cos(np.deg2rad(lat)).clip(0., 1.)
    wgts = np.sqrt(coslat)[..., np.newaxis]
    solver = Eof(slp, weights=wgts)

    # Retrieve the leading EOF, expressed as the covariance between the leading PC
    # time series and the input SLP anomalies at each grid point.
    
    eof1 = solver.eofsAsCovariance(pcscaling=0, neofs=eof_number)
    variance_fractions = solver.varianceFraction()
    pc1 = solver.pcs(npcs=eof_number, pcscaling=1) # Unit variance

    return eof1.squeeze(), variance_fractions, pc1


def track_center_kMeans(eof, ref, variance_fractions, n = 15):
    """
    Applies kMeans cluster algorithm to n-EOF field. Correct EOF is idetified
    by performing spatial correlation with reference EOF.

    Arguments:
        eof {float} -- n-EOFS
        ref {float} -- reference EOF
        variance_fractions {[type]} -- [description]
    
    Keyword Arguments:
        n {int} -- number of clusters (default: {15})
    
    Returns:
        [4 tuple] -- Cluster minimum, maximum coordinates, EOF that matches reference EOF,
                     explained variance of EOF
    """

    check = None

    import numpy as np
    from sklearn.cluster import KMeans
    from corr2 import corr2    
    import pylab as pl

    for count, tmp in enumerate(eof):

        #
        # Calculate EOF (SLP). Corr is used to find similar pattern to NAO+ or NAO- of reference.
        #
        
        tmp_clust = tmp.reshape(-1,1)
        k_means = KMeans(n_clusters=n, random_state= 0)
        _ = k_means.fit(tmp_clust)
        X_clustered = k_means.labels_
        X_clustered = X_clustered.reshape(tmp.shape)


        label_list = [tmp[X_clustered == i].mean() for i in np.arange(0,n)]
        mean_sort = np.argsort(np.asarray(label_list))
        
        new = X_clustered.copy().astype(float)

        for step, i in enumerate(mean_sort):
            fac = step * 0.1
            new[X_clustered == i] = fac
        new = (new * 10).astype(int)
        corr = corr2(tmp, ref[0])

        # f, (ax, bx) = pl.subplots(2)
        # im = ax.pcolor(new)
        # im2 = bx.pcolor(ref[0])
        # pl.colorbar(im, ax = ax)
        # pl.colorbar(im2, ax = bx)
        # pl.pause(1)
        # pl.close()

        if corr >= 0.75:
            lats, lons = np.where(new == 0)
            lats2, lons2 = np.where(new == n-1)
            minimum = np.nanmean(lats), np.nanmean(lons)
            maximum = np.nanmean(lats2), np.nanmean(lons2)
            return minimum, maximum, tmp, variance_fractions[count]

        elif corr <= -0.75:
            lats, lons = np.where(new == n-1)
            lats2, lons2 = np.where(new == 0)
            minimum = np.nanmean(lats), np.nanmean(lons)
            maximum = np.nanmean(lats2), np.nanmean(lons2)
            return minimum, maximum, tmp, variance_fractions[count]
        
    return np.nan, np.nan, np.nan, np.nan

def calc_wtc(df_center_shift, amo, verbose = False):
    import numpy as np
    import pylab as pl
    import pandas as pd
    import warnings
    import pycwt as wavelet
    warnings.filterwarnings("ignore",category =RuntimeWarning)

    def plot_parameters():
        import pylab as pl
        params = {'lines.linewidth':'2',
        'text.usetex': True,
        'legend.fontsize': '30',
        'axes.labelsize': '35',
        'axes.titlesize':'x-large',
        'xtick.labelsize':'35',
        'ytick.labelsize':'35',
        'font.size':'30'}
        return params

    #
    # Load all variables
    #

    #
    # Init wavelet coherence
    #

    mother    = "morlet"
    dt        = 1
    t0        = 950
    s0        = 2 * dt
    dj        = 1 / 12
    J         = 7 / dj
    n         = len(amo)
    time      = np.arange(n) * dt + t0
    test_case = True               # If test case, monte carlo simulations for significance testing are set to 3

    if test_case == True:
        mcs = 5
    else:
        mcs = 20


    #
    # start wavelet coherence routine
    #

    params = plot_parameters()

    pl.rcParams.update(params)

    fig, ax = pl.subplots(2, 2, figsize =(25, 15), sharex='col', sharey='row')
    ax = ax.ravel()

    for counter, center in enumerate(df_center_shift):

        print(center)

        #
        # Calculation
        #

        current_center = df_center_shift[center].values

        WCT, aWCT, corr_coi, freq, sig = wavelet.wct(amo, current_center, dt,
                                                    dj=dj, s0=s0, J=-1,
                                                    significance_level=0.8646,
                                                    wavelet=mother, normalize=True,
                                                    cache=False, mc_count = mcs)
        cor_sig = np.ones([1, n]) * sig[:, None]
        cor_sig = np.abs(WCT) / cor_sig     # Power is significant where ratio > 1
        cor_period = 1 / freq

        angle = 0.5 * np.pi - aWCT          # Phase relationship
        u, v = np.cos(angle), np.sin(angle)  

        u_sig = u.copy()                    # Mask everything that's not significant
        v_sig = v.copy()

        u_sig[cor_sig<1] = np.ma.masked
        v_sig[cor_sig<1] = np.ma.masked

        [t1_sig,cor_period_sig] = np.meshgrid(time,cor_period)
        t1_sig[cor_sig<1] = np.ma.masked
        cor_period_sig[cor_sig<1] = np.ma.masked
        
        #
        # Plotting
        #
        uv = [u,v]
        uv_sig = [u_sig, v_sig]

        levels = np.linspace(0,1,11)
        ax[counter].set_title('AMO and {}'.format(center))
        im2 = ax[counter].contourf(time, np.log2(cor_period)[1:90], WCT[1:90], cmap=pl.cm.Blues, levels=levels, antialiased=True, alpha=0.8)
        ax[counter].contour(time, np.log2(cor_period[1:90]), cor_period_sig[1:90], [-99, 1], colors='k', linewidths=2)
        ax[counter].quiver(t1_sig[35:90][::3,::30], np.log2(cor_period_sig[35:90][::3,::30]), uv_sig[0][35:90][::3,::30],uv_sig[1][35:90][::3,::30], units='height',\
        angles='uv', pivot='mid', linewidth=1, edgecolor='k',scale=35,scale_units="height",
        headwidth=5, headlength=5, headaxislength=2, minshaft=2,
        minlength=2)
        ax[counter].fill(np.concatenate([time, time[-1:] + dt,\
                time[-1:] + dt,
                time[:1] - dt, time[:1] - dt]),
                np.concatenate([np.log2(corr_coi), [1e-9],\
                np.log2(cor_period[-1:]),
                np.log2(cor_period[-1:]), [1e-9]]),
                'k', alpha=0.7, hatch='x')
        ax[counter].set_yticks([1,2,3,4,5,6,7,8])
        ax[counter].set_ylim([1,8])
        ax[counter].set_yticklabels([2,4,8,16,32,64,128,256])
        ax[counter].invert_yaxis()
        ax[counter].set_xlim([950, 1800])

    ax[2].set_xlabel('Time in years')
    ax[0].set_ylabel('Period in years')
    ax[3].set_xlabel('Time in years')
    ax[2].set_ylabel('Period in years')

    fig.subplots_adjust(right=0.85)
    cbar_ax = fig.add_axes([0.9, 0.15, 0.05, 0.7])
    cbar = fig.colorbar(im2, cax=cbar_ax)
    cbar.set_ticks([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
    cbar.ax.set_ylabel('Coherence')
    pl.savefig('../../figures/wavelets_figure2_new.png', dpi = 300)
    if verbose == True:
        pl.show()
    pl.close()

def plot_scatter_3d(df_eofs, size=(15, 4)):
    import numpy as np
    import matplotlib.pyplot as pl
    from mpl_toolkits.mplot3d import Axes3D

    #
    # (Subplot 1)
    #

    fig = pl.figure(figsize=size)

    U, V = df_eofs["Azores High Longitude"].rolling(100).mean().values, df_eofs["Azores High Latitude"].rolling(100).mean().values
    time_axis = np.arange(len(df_eofs["Azores High Latitude"].values)) + 950 

    ax = fig.add_subplot(121, projection='3d')
    ax.scatter(U,V, time_axis, c=time_axis)
    ax.set_title('Azores High')
    ax.set_xlabel('Longitude')
    ax.set_ylabel('Latitude')
    ax.set_zlabel('Time')
    ax.elev = 30
    ax.azim = 30

    # Get rid of colored axes planes
    # First remove fill
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False

    # Now set color to white (or whatever is "invisible")
    ax.xaxis.pane.set_edgecolor('w')
    ax.yaxis.pane.set_edgecolor('w')
    ax.zaxis.pane.set_edgecolor('w')

    U, V = df_eofs["Icelandic Low Longitude"].rolling(100).mean().values, df_eofs["Icelandic Low Latitude"].rolling(100).mean().values

    x = np.arange(df_eofs["Icelandic Low Longitude"].values.min(), df_eofs["Icelandic Low Longitude"].values.max())
    y = np.arange(df_eofs["Icelandic Low Latitude"].values.min(), df_eofs["Icelandic Low Latitude"].values.max())

    #
    # (Subplot 2)
    #
    ax = fig.add_subplot(122, projection='3d')
    ax.set_title('Icelandicic Low')
    ax.scatter(U, V, time_axis, c=time_axis)
    ax.set_xlabel('Longitude')
    ax.set_ylabel('Latitude')
    ax.set_zlabel('Time')
    ax.elev = 30
    ax.azim = 30

    # Get rid of colored axes planes
    # First remove fill
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False

    # Now set color to white (or whatever is "invisible")
    ax.xaxis.pane.set_edgecolor('w')
    ax.yaxis.pane.set_edgecolor('w')
    ax.zaxis.pane.set_edgecolor('w')

    pl.savefig('../../figures/center_shift.png', dpi = 300)
    pl.close()

def calc_amo(begin, end, interpolation_method=1, do_roll = True):
    """
    Calculate AMO

    Both datasets are based on ECHO-G with some missing years.
    Therefore we interpolate the missing values.
    Missing values at the boundaries are calculated by using bfill
    and ffill.
    """

    import netCDF4
    import pandas as pd
    import numpy as np

    dset_sst_global = netCDF4.Dataset("../../data/oetzi2_sst_yearmean.nc")
    dset_amo = netCDF4.Dataset("../../data/oetzi2_sst_-80_0_0_60.nc")

    amo = dset_amo.variables['var169'][-10+begin:end,0,0]
    sst_global = dset_sst_global.variables['var169'][-10+begin:end,0,0]
    
    amo =  amo - sst_global # Remove global trend

    amo_corrected = amo.copy()
    
    amo_corrected = interpolate_data(amo_corrected, verbose = False, interpolation_method=interpolation_method)

    if do_roll == True:
        dataFrame_amo = pd.DataFrame(amo_corrected,columns=['Mean SST Atlantic'])
        dataFrame_amo = dataFrame_amo.rolling(10,min_periods = 1).mean()
        amo_corrected = dataFrame_amo.values.squeeze()

    amo = amo_corrected[10:]
    amo = amo - np.nanmean(amo) # anomaly

    return amo

def plot_amo_center_shift(dset, lon = "xt_ocean", lat = "yt_ocean",lon_range = None, lat_range=None,
                          reverse = False, option = "cyl", lon0 = 0., lat0 = 67.0):
    """
    Creates map of the Baltic Sea.
    """
    from mpl_toolkits.basemap import Basemap
    import numpy as np

    """
    lon = dset.variables['lon'][2:36]
    lat = dset.variables['lat'][2:]    
    """

    lon = dset.variables[lon][:]
    if reverse == True:
        lat = dset.variables[lat][:]
        lat = lat[::-1]
    else:
        lat = dset.variables[lat][:]   

    if lon_range:
        lon = lon[lon_range[0]:lon_range[1]]
    if lat_range:
        lat = lat[lat_range[0]:lat_range[1]]

    LON, LAT = np.meshgrid(lon, lat)
    
    if option == 'cyl':

        BA_LON = [lon.min(),lon.max()]
        BA_LAT = [lat.min(),lat.max()]
        cp = (.710,.75,.75)

        ba = Basemap(projection='cyl',    
                resolution= 'h',         
                    area_thresh=100.,     
                    lat_0=35.00,           
                    lon_0=20.0,       
                    lat_1=37.75,            
                    llcrnrlon=BA_LON[0],          
                    llcrnrlat=BA_LAT[0],           
                    urcrnrlon=BA_LON[1],                
                    urcrnrlat=BA_LAT[1])
    if option == 'ortho':

        from mpl_toolkits.basemap import Basemap
        import matplotlib.pyplot as plt

        fig = plt.figure(figsize = (5,5))

        ba = Basemap(projection = 'ortho', lon_0 = lon0, lat_0 = lat0)

        BA_LON = [lon.min()-5,lon.max()+5]
        BA_LAT = [lat.min()-5,lat.max()+5]

        tmp1 = ba(BA_LON[0], BA_LON[1])
        tmp2 = ba(BA_LAT[0], BA_LAT[1])
        
        x_range = [tmp1[0], tmp1[1]]
        y_range = [tmp2[0], tmp2[1]]

        return x_range, y_range, ba
       
    return LON, LAT, ba

def autocorr(self, lag=1):
    """
    Lag-N autocorrelation

    Parameters
    ----------
    lag : int, default 1
        Number of lags to apply before performing autocorrelation.

    Returns
    -------
    autocorr : float
    """
    return self.corr(self.shift(lag))

def crosscorr(datax, datay, lag=0):
    """ Lag-N cross correlation. 
    Parameters
    ----------
    lag : int, default 0
    datax, datay : pandas.Series objects of equal length

    Returns
    ----------
    crosscorr : float
    """
    return datax.corr(datay.shift(lag))

def convert_min_max(minimum, maximum, lon, lat):
    import numpy as np
    import pandas as pd

    lon_list_min = [i[1] if ~np.isnan(i).any() else np.nan for i in minimum] 
    lat_list_min = [i[0] if ~np.isnan(i).any() else np.nan for i in minimum]
    lon_list_max = [i[1] if ~np.isnan(i).any() else np.nan for i in maximum]
    lat_list_max = [i[0] if ~np.isnan(i).any() else np.nan for i in maximum]

    lon_lists_max_yr = np.asarray(lon_list_max).reshape(-1, 4)
    lon_lists_max_yr = np.nanmean(lon_lists_max_yr, axis = 1)[:]

    lon_lists_min_yr = np.asarray(lon_list_min).reshape(-1, 4)
    lon_lists_min_yr = np.nanmean(lon_lists_min_yr, axis = 1)[:]

    lat_lists_max_yr = np.asarray(lat_list_max).reshape(-1, 4)
    lat_lists_max_yr = np.nanmean(lat_lists_max_yr, axis = 1)[:]

    lat_lists_min_yr = np.asarray(lat_list_min).reshape(-1, 4)
    lat_lists_min_yr = np.nanmean(lat_lists_min_yr, axis = 1)[:]

    lons_min = [lon[int(x)] + x % 1 * 3.75 for x in lon_lists_min_yr]
    lons_max = [lon[int(x)] + x % 1 * 3.75 for x in lon_lists_max_yr]
    lats_min = [lat[int(x)] + x % 1 * 3.75 for x in lat_lists_min_yr]
    lats_max = [lat[int(x)] + x % 1 * 3.75 for x in lat_lists_max_yr]

    df_eofs = pd.DataFrame({"lon lists min": lons_min,
                            "lat lists min": lats_min,
                            "Icelandic Low Longitude": lons_max,
                            "Icelandic Low Latitude": lats_max})
    
    return df_eofs

def calc_nao(dset, slp_corrected, time_overlap):
    import pandas as pd
    import numpy as np
    _, _ , pcas = do_eof(dset, slp_corrected, do_anomaly = True, eof_number=4)
    nao = pcas[:-time_overlap,0] # Overlap
    nao_yr = np.nanmean(nao.reshape(-1, 4), axis = 1) 
    time = np.arange(950, 1799)
    return pd.DataFrame({"NAO EOF(1)": nao_yr}, index = time)

def plot_nao_centershift(df, xcov_pre,xcov_pre_i):
    import pylab as pl
    import numpy as np
    f, ax = pl.subplots(2, 1, figsize=(25, 25))
    ax = ax.ravel()
    df.plot(ax = ax[0])
    ax[1].plot(np.arange(-50,50), xcov_pre, label = "azores")
    ax[1].plot(np.arange(-50,50), xcov_pre_i, label = "ice")
    pl.xlabel('shift of NAO in Years')
    pl.ylabel('Correlation')
    pl.title('Cross correlation AMO - NAO, 10 yr mean applied')
    pl.savefig("../../figures/amo_azores_low_cross_cor_10yr.png")
    pl.show()
    pl.close()

def anglemean(theta):
    """
    Calculates the mean of angles

    see:
    http://www.cosy.sbg.ac.at/~reini/huber_dutra_freitas_igarss_01.pd

    Arguments:
        theta {[type]} -- [description]
    
    Returns:
        anglestrength: can be thought of as the inverse variance. [varies between 0 and one]
        sigma: circular standard deviation
        confangle: a 95% confidence angle (confidence of the mean value)
        kappa: an estimate of kappa in the Von Mises distribution

    Adapted for python by Florian Börgel
    Copyright (c) 2014 Aslak Grinsted
    """

    import numpy as np

    theta = np.mod(theta, 2*np.pi)
    n     = len(theta)
    S     = np.sum(np.sin(theta), axis = 0)
    C     = np.sum(np.cos(theta), axis = 0)

    theta_m = np.arctan2(S,C)

    Rsum_a = np.sqrt(S**2+C**2)
    R_a    = Rsum_a/n

    kappa = np.zeros(R_a.shape)
    confangle = np.zeros(R_a.shape)

    for i, R in enumerate(R_a):
        if (R < 0.53):
            kappa[i] = 2*R+R**3+5*R**5/6
        elif (R < 0.85):
            kappa[i] = -0.4 + 1.39 * R + 0.43 / (1-R)
        else:
            kappa[i] = 1 / (R**3-4*R**2+3*R)

        # circular standard deviation

        sigma = np.sqrt(-2 * np.log(R))

        chi2 = 3.841
        if ((R < 0.95 ) and (R > np.sqrt(chi2 / (2*n)))):
            confangle[i] = np.arccos(np.sqrt(2*n*(2*Rsum_a[i]**2-n*chi2)/(4*n-chi2))/Rsum_a[i])
            print(i)
        elif (R > 0.9):
            confangle[i] = np.arccos(np.sqrt(n**2-(n**2-Rsum_a[i]**2)*np.exp(chi2/n))/Rsum_a[i])
        else:
            confangle[i] = np.pi / 2
            print('Confidence angle not well determined.')
    
    return theta_m, R, sigma, confangle, kappa
    
