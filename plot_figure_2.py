""" Creates figures and stores it as .png """

__author__ = "Florian Boergel"
__email__  = "florian.boergel@io-warnemuende.de"

import xarray as xr
import pylab as plt
import pandas as pd

import cartopy.crs as ccrs
import matplotlib.ticker as mticker
import cartopy.feature as cfeature
from matplotlib.offsetbox import AnchoredText

from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

from revisions_functions import amo


overlaps = ["overlapping_eof_centers_timeoverlap_20.nc",
            "overlapping_eof_centers_timeoverlap_30.nc",
            "overlapping_eof_centers_timeoverlap_40.nc",
            "overlapping_eof_centers_timeoverlap_50.nc"]


ds = xr.open_mfdataset(overlaps, concat_dim = "ensemble")

print(ds)

begin_mca = 5950 + 1500
end_mca   = 6250 + 1500

begin_lia = 6300 + 1500
end_lia   = 6799 + 1500

# Medieval Climate Anomaly (MCA)

ds_mca = ds.sel(year=slice(begin_mca,end_mca))
ds_lia = ds.sel(year=slice(begin_lia,end_lia))

amo_calc = xr.open_dataset("amo_full.nc")
amo_time_mca = amo_calc.amo.sel(year=slice(begin_mca,end_mca))
amo_time_lia = amo_calc.amo.sel(year=slice(begin_lia,end_lia))

std_mca = amo_time_mca.std()
std_lia= amo_time_lia.std()

threshold_mca = 0.5*std_mca*0
threshold_lia = 0.5*std_lia*0

amo_plus_mca  = amo_time_mca[amo_time_mca > threshold_mca]
amo_minus_mca = amo_time_mca[amo_time_mca < -threshold_mca]

amo_plus_lia = amo_time_lia[amo_time_lia > threshold_lia]
amo_minus_lia = amo_time_lia[amo_time_lia < -threshold_lia]

# Figure 1 (MCA)

f, ax = plt.subplots(4,1,subplot_kw={'projection': ccrs.PlateCarree()}, figsize=(15,15))

for count, ensemble_number in enumerate(ds_mca.ensemble.values):
    amo_plus_centers = ds_mca.sel(ensemble=ensemble_number).loc[dict(year=amo_plus_mca.year)]
    amo_minus_centers = ds_mca.sel(ensemble=ensemble_number).loc[dict(year=amo_minus_mca.year)]

    central_lon, central_lat = -10, 45
    extent = [-40, 20, 35, 75]
    ax[count].set_extent(extent)
    ax[count].coastlines(resolution='50m')

    ax[count].plot(amo_plus_centers["minimum_center_position_lon"], amo_plus_centers["minimum_center_position_lat"],
              color='red', linewidth=0, marker='o', alpha=0.3,
             transform=ccrs.PlateCarree())

    ax[count].plot(amo_plus_centers["minimum_center_position_lon"].mean(), amo_plus_centers["minimum_center_position_lat"].mean(),
            color='black', linewidth=0, marker='X', alpha=1, markersize=16,
            transform=ccrs.PlateCarree())

    ax[count].plot(amo_plus_centers["maximum_center_position_lon"], amo_plus_centers["maximum_center_position_lat"],
             color='red', linewidth=0, marker='o', alpha=0.3,
             transform=ccrs.PlateCarree())

    ax[count].plot(amo_minus_centers["minimum_center_position_lon"], amo_minus_centers["minimum_center_position_lat"],
              color='blue', linewidth=0, marker='x', alpha=0.3,
             transform=ccrs.PlateCarree())

    ax[count].plot(amo_minus_centers["minimum_center_position_lon"].mean(), amo_minus_centers["minimum_center_position_lat"].mean(),
            color='black', linewidth=0, marker='X', alpha=1, markersize=16,
            transform=ccrs.PlateCarree())
    
    
    ax[count].plot(amo_minus_centers["maximum_center_position_lon"], amo_minus_centers["maximum_center_position_lat"],
             color='blue', linewidth=0, marker='x', alpha=0.3,
             transform=ccrs.PlateCarree())
    
    gl = ax[count].gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                    linewidth=2, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_left = False
    gl.xlines = True
    gl.ylines = False
    gl.xlocator = mticker.FixedLocator([-40, -30, -30, -20, -10, 0, 10, 20])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': 15, 'color': 'gray'}
    gl.xlabel_style = {'color': 'black','weight':"bold"}
    gl.ylabel_style = {'size': 15, 'color': 'gray'}
    gl.ylabel_style = {'color': 'black', 'weight':"bold"}

    ax[count].annotate('AMO-', c="blue",
            xy=(amo_minus_centers["minimum_center_position_lon"].mean(), amo_minus_centers["minimum_center_position_lat"].mean()), xycoords='data',
            xytext=(amo_minus_centers["minimum_center_position_lon"].mean()-50, amo_minus_centers["minimum_center_position_lat"].mean()-10), textcoords='offset points',
            bbox=dict(boxstyle="round", fc="0.8"),
            arrowprops=dict(arrowstyle="->",shrinkA=0, shrinkB=10,
                            connectionstyle="angle,angleA=0,angleB=90,rad=10"),
            size=15)

    ax[count].annotate('AMO+', c="red",
            xy=(amo_plus_centers["minimum_center_position_lon"].mean(), amo_plus_centers["minimum_center_position_lat"].mean()), xycoords='data',
            xytext=(amo_plus_centers["minimum_center_position_lon"].mean()+50, amo_plus_centers["minimum_center_position_lat"].mean()-10), textcoords='offset points',
            bbox=dict(boxstyle="round", fc="0.8"),
            arrowprops=dict(arrowstyle="->",
                            shrinkA=0, shrinkB=10,
                            connectionstyle="angle,angleA=90,angleB=0,rad=10"),
            size=15)


    ax[count].add_feature(cfeature.LAND)


    
ax[0].add_artist(AnchoredText("EOF overlap of 20 yr",
                    loc=1, prop={'size': 12}, frameon=True))
ax[1].add_artist(AnchoredText("EOF overlap of 30 yr",
                    loc=1, prop={'size': 12}, frameon=True))
ax[2].add_artist(AnchoredText("EOF overlap of 40 yr",
                    loc=1, prop={'size': 12}, frameon=True))
ax[3].add_artist(AnchoredText("EOF overlap of 50 yr",
                    loc=1, prop={'size': 12}, frameon=True))

ax[0].set_title("Medieval Climate Anomaly")

plt.savefig("mca_shift_map.png", dpi = 300)


f, ax = plt.subplots(4,1,subplot_kw={'projection': ccrs.PlateCarree()}, figsize=(15,15))

for count, ensemble_number in enumerate(ds_lia.ensemble.values):
    amo_plus_centers = ds_lia.sel(ensemble=ensemble_number).loc[dict(year=amo_plus_lia.year)]
    amo_minus_centers = ds_lia.sel(ensemble=ensemble_number).loc[dict(year=amo_minus_lia.year)]

    central_lon, central_lat = -10, 45
    extent = [-40, 20, 35, 75]
    ax[count].set_extent(extent)
    ax[count].coastlines(resolution='50m')

    ax[count].plot(amo_plus_centers["minimum_center_position_lon"], amo_plus_centers["minimum_center_position_lat"],
              color='red', linewidth=0, marker='o', alpha=0.3,
             transform=ccrs.PlateCarree())

    ax[count].plot(amo_plus_centers["minimum_center_position_lon"].mean(), amo_plus_centers["minimum_center_position_lat"].mean(),
            color='black', linewidth=0, marker='X', alpha=1, markersize=16,
            transform=ccrs.PlateCarree())

    ax[count].plot(amo_plus_centers["maximum_center_position_lon"], amo_plus_centers["maximum_center_position_lat"],
             color='red', linewidth=0, marker='o', alpha=0.3,
             transform=ccrs.PlateCarree())

    ax[count].plot(amo_minus_centers["minimum_center_position_lon"], amo_minus_centers["minimum_center_position_lat"],
              color='blue', linewidth=0, marker='x', alpha=0.3,
             transform=ccrs.PlateCarree())

    ax[count].plot(amo_minus_centers["minimum_center_position_lon"].mean(), amo_minus_centers["minimum_center_position_lat"].mean(),
            color='black', linewidth=0, marker='X', alpha=1, markersize=16,
            transform=ccrs.PlateCarree())
    
    
    ax[count].plot(amo_minus_centers["maximum_center_position_lon"], amo_minus_centers["maximum_center_position_lat"],
             color='blue', linewidth=0, marker='x', alpha=0.3,
             transform=ccrs.PlateCarree())
    
    gl = ax[count].gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                    linewidth=2, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_left = False
    gl.xlines = True
    gl.ylines = False
    gl.xlocator = mticker.FixedLocator([-40, -30, -30, -20, -10, 0, 10, 20])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': 15, 'color': 'gray'}
    gl.xlabel_style = {'color': 'black','weight':"bold"}
    gl.ylabel_style = {'size': 15, 'color': 'gray'}
    gl.ylabel_style = {'color': 'black', 'weight':"bold"}

    ax[count].annotate('AMO-', c="blue",
            xy=(amo_minus_centers["minimum_center_position_lon"].mean(), amo_minus_centers["minimum_center_position_lat"].mean()), xycoords='data',
            xytext=(amo_minus_centers["minimum_center_position_lon"].mean()+50, amo_minus_centers["minimum_center_position_lat"].mean()-10), textcoords='offset points',
            bbox=dict(boxstyle="round", fc="0.8"),
            arrowprops=dict(arrowstyle="->",shrinkA=0, shrinkB=10,
                            connectionstyle="angle,angleA=0,angleB=90,rad=10"),
            size=15)

    ax[count].annotate('AMO+', c="red",
            xy=(amo_plus_centers["minimum_center_position_lon"].mean(), amo_plus_centers["minimum_center_position_lat"].mean()), xycoords='data',
            xytext=(amo_plus_centers["minimum_center_position_lon"].mean()-50, amo_plus_centers["minimum_center_position_lat"].mean()-10), textcoords='offset points',
            bbox=dict(boxstyle="round", fc="0.8"),
            arrowprops=dict(arrowstyle="->",
                            shrinkA=0, shrinkB=10,
                            connectionstyle="angle,angleA=90,angleB=0,rad=10"),
            size=15)
    ax[count].add_feature(cfeature.LAND)

    


ax[0].set_title("Little Ice Age")
ax[0].add_artist(AnchoredText("EOF overlap of 20 yr",
                    loc=1, prop={'size': 12}, frameon=True))
ax[1].add_artist(AnchoredText("EOF overlap of 30 yr",
                    loc=1, prop={'size': 12}, frameon=True))
ax[2].add_artist(AnchoredText("EOF overlap of 40 yr",
                    loc=1, prop={'size': 12}, frameon=True))
ax[3].add_artist(AnchoredText("EOF overlap of 50 yr",
                    loc=1, prop={'size': 12}, frameon=True))

plt.show()
plt.savefig("lia_shift_map.png", dpi = 300)

