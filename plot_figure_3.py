""" Creates figures and stores it as .png """

__author__ = "Florian Boergel"
__email__  = "florian.boergel@io-warnemuende.de"

import xarray as xr
import pylab as plt
import pandas as pd

from revisions_functions import amo

from windrose import WindroseAxes
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as mtick

import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42


import windrose
import numpy as np

ds = xr.open_mfdataset("overlapping_eof_centers_timeoverlap_*.nc", concat_dim = "ensemble")

offset = 1500

ds = ds.sel(year=slice(5950+offset,6250+offset))
amo_time = xr.open_dataset("amo_full.nc")
threshold = amo_time.amo.std() * 0.25 *0
# Calculate velocity by calculating dt

diff_lon = ds.sel(ensemble=3)["minimum_center_position_lon"].dropna("year").diff("year")
diff_lat = ds.sel(ensemble=3)["minimum_center_position_lat"].dropna("year").diff("year")

amo_plus  = amo_time.sel(year=slice(5951+offset,6250+offset)).amo[amo_time.sel(year=slice(5951+offset,6250+offset)).amo >= threshold]
amo_minus = amo_time.sel(year=slice(5951+offset,6250+offset)).amo[amo_time.sel(year=slice(5951+offset,6250+offset)).amo < -threshold]

# sort by amo+ and amo-

diff_lon_plus = diff_lon.loc[(dict(year=amo_plus.year))]
diff_lat_plus = diff_lat.loc[(dict(year=amo_plus.year))]

diff_lon_minus = diff_lon.loc[(dict(year=amo_minus.year))]
diff_lat_minus = diff_lat.loc[(dict(year=amo_minus.year))]

# direction amo+ and amo-
d_amo_plus = (180/np.pi)*(xr.ufuncs.arctan2(diff_lon_plus, diff_lat_plus)).load() 
d_amo_minus = (180/np.pi)*(xr.ufuncs.arctan2(diff_lon_minus, diff_lat_minus)).load()

d_amo_plus[d_amo_plus < 0] = d_amo_plus[d_amo_plus < 0] + 360
d_amo_minus[d_amo_minus < 0] = d_amo_minus[d_amo_minus < 0] + 360

## abs velocity amo and amo-

ws_amo_plus = (diff_lon_plus**2+diff_lat_plus**2)**0.5
ws_amo_plus = ws_amo_plus.loc[dict(year=d_amo_plus.year.values)] 

ws_amo_minus = (diff_lon_minus**2+diff_lat_minus**2)**0.5
ws_amo_minus = ws_amo_minus.loc[dict(year=d_amo_minus.year.values)] 

ws_amo_minus_std = ws_amo_minus.std()
ws_amo_plus_std = ws_amo_plus.std()

ws_amo_plus_new = ws_amo_plus.where(ws_amo_plus > ws_amo_plus_std, drop=True)
ws_amo_minus_new = ws_amo_minus.where(ws_amo_minus > ws_amo_minus_std, drop=True)

d_amo_minus = d_amo_minus.where(ws_amo_minus > ws_amo_minus_std, drop=True)
d_amo_plus = d_amo_plus.where(ws_amo_plus > ws_amo_plus_std, drop=True)

print(ws_amo_plus_new.values)

fig = plt.figure(figsize = (15,10))

axis = fig.add_subplot(221, projection="windrose")
axis.bar(d_amo_plus.values, ws_amo_plus_new.values, bins=np.arange(0,10,1), cmap=plt.cm.Blues, normed = True, nsector=8)
axis.set_title("AMO+")
axis.set_yticks(np.arange(0, 65, step=10))
axis.set_yticklabels(np.arange(0, 65, step=10))
axis.yaxis.set_major_formatter(mtick.PercentFormatter())

axis = fig.add_subplot(223, projection="windrose")
axis.bar(d_amo_minus.values, ws_amo_minus_new.values,bins=np.arange(0,10,1), cmap=plt.cm.Blues, normed=True,nsector=8)
axis.set_title("AMO-")
axis.set_yticks(np.arange(0, 65, step=10))
axis.set_yticklabels(np.arange(0, 65, step=10))
axis.yaxis.set_major_formatter(mtick.PercentFormatter())


ds = xr.open_mfdataset("overlapping_eof_centers_timeoverlap_*.nc", concat_dim = "ensemble")

ds = ds.sel(year=slice(6300+offset,6799+offset))
amo_time = xr.open_dataset("amo_full.nc")

threshold = amo_time.amo.std() * 0.25 * 0

# Calculate velocity by calculating dt

diff_lon = ds.sel(ensemble=3)["minimum_center_position_lon"].dropna("year").diff("year")
diff_lat = ds.sel(ensemble=3)["minimum_center_position_lat"].dropna("year").diff("year")

amo_plus  = amo_time.sel(year=slice(6301+offset,6799+offset)).amo[amo_time.sel(year=slice(6301+offset,6799+offset)).amo >= threshold]
amo_minus = amo_time.sel(year=slice(6301+offset,6799+offset)).amo[amo_time.sel(year=slice(6301+offset,6799+offset)).amo < -threshold]

# sort by amo+ and amo-

diff_lon_plus = diff_lon.loc[(dict(year=amo_plus.year))]
diff_lat_plus = diff_lat.loc[(dict(year=amo_plus.year))]

diff_lon_minus = diff_lon.loc[(dict(year=amo_minus.year))]
diff_lat_minus = diff_lat.loc[(dict(year=amo_minus.year))]

# direction amo+ and amo-
d_amo_plus = (180/np.pi)*(xr.ufuncs.arctan2(diff_lon_plus, diff_lat_plus)).load() 
d_amo_minus = (180/np.pi)*(xr.ufuncs.arctan2(diff_lon_minus, diff_lat_minus)).load()

d_amo_plus[d_amo_plus < 0] = d_amo_plus[d_amo_plus < 0] + 360
d_amo_minus[d_amo_minus < 0] = d_amo_minus[d_amo_minus < 0] + 360

## abs velocity amo and amo-

ws_amo_plus = (diff_lon_plus**2+diff_lat_plus**2)**0.5
ws_amo_plus = ws_amo_plus.loc[dict(year=d_amo_plus.year.values)] 

ws_amo_minus = (diff_lon_minus**2+diff_lat_minus**2)**0.5
ws_amo_minus = ws_amo_minus.loc[dict(year=d_amo_minus.year.values)] 

ws_amo_minus_std = ws_amo_minus.std()
ws_amo_plus_std = ws_amo_plus.std()

ws_amo_plus_new = ws_amo_plus.where(ws_amo_plus > ws_amo_plus_std, drop=True)
ws_amo_minus_new = ws_amo_minus.where(ws_amo_minus > ws_amo_minus_std, drop=True)

d_amo_minus = d_amo_minus.where(ws_amo_minus > ws_amo_minus_std, drop=True)
d_amo_plus = d_amo_plus.where(ws_amo_plus > ws_amo_plus_std, drop=True)


axis = fig.add_subplot(222, projection="windrose")
axis.bar(d_amo_plus.values, ws_amo_plus_new.values, bins=np.arange(0,10,1), cmap=plt.cm.Blues, normed = True, nsector=8)
axis.set_title("AMO+")
axis.set_yticks(np.arange(0, 65, step=10))
axis.set_yticklabels(np.arange(0, 65, step=10))
axis.yaxis.set_major_formatter(mtick.PercentFormatter())

axis = fig.add_subplot(224, projection="windrose")
axis.bar(d_amo_minus.values, ws_amo_minus_new.values,bins=np.arange(0,10,1), cmap=plt.cm.Blues, normed=True,nsector=8)

axis.set_title("AMO-")

axis.set_yticks(np.arange(0, 65, step=10))
axis.set_yticklabels(np.arange(0, 65, step=10))
axis.yaxis.set_major_formatter(mtick.PercentFormatter())

axes = plt.gcf().get_axes()
for ax in axes:
    ax.set_aspect('equal', 'box')

plt.savefig("windrose_mca_lia.png", dpi = 300)

